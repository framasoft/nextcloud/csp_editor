/*
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { createAppConfig } from '@nextcloud/vite-config'

export default createAppConfig({
	// entry points: {name: script}
	'admin-settings': 'src/admin.js',
})
