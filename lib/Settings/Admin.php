<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\CSPEditor\Settings;

use OCA\CSPEditor\AppInfo\Application;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IAppConfig;
use OCP\IConfig;
use OCP\Settings\ISettings;
use OCP\Util;

class Admin implements ISettings {

	public function __construct(
		private IConfig $config,
		private IAppConfig $appConfig,
		private IInitialState $initialState,
	) {
	}

	public function getForm(): TemplateResponse {
		$config = $this->appConfig->getValueString(Application::APP_NAME, 'customCSP', '[]');
		$debug = $this->config->getSystemValueBool('debug', false);

		$this->initialState->provideInitialState('customCSP', $config);
		$this->initialState->provideInitialState('debug', $debug);
		Util::addScript(Application::APP_NAME, 'csp_editor-admin-settings');
		Util::addStyle(Application::APP_NAME, 'csp_editor-admin-settings');

		return new TemplateResponse(Application::APP_NAME, 'admin');
	}

	public function getSection(): string {
		return 'additional';
	}

	public function getPriority(): int {
		return 80;
	}
}
