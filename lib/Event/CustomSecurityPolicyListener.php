<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\CSPEditor\Event;

use OCA\CSPEditor\Service\CustomSecurityPolicyBuilder;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\Security\CSP\AddContentSecurityPolicyEvent;

/**
 * @template-implements IEventListener<AddContentSecurityPolicyEvent>
 */
class CustomSecurityPolicyListener implements IEventListener {
	private CustomSecurityPolicyBuilder $customSecurityPolicyBuilder;

	public function __construct(CustomSecurityPolicyBuilder $customSecurityPolicyBuilder) {
		$this->customSecurityPolicyBuilder = $customSecurityPolicyBuilder;
	}

	public function handle(Event $event): void {
		if (!$event instanceof AddContentSecurityPolicyEvent) {
			return;
		}

		$policy = $this->customSecurityPolicyBuilder->buildPolicy();

		$event->addPolicy($policy);
	}
}
