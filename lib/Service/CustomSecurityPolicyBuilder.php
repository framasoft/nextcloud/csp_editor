<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\CSPEditor\Service;

use OCA\CSPEditor\AppInfo\Application;
use OCP\AppFramework\Http\EmptyContentSecurityPolicy;
use OCP\Exceptions\AppConfigTypeConflictException;
use OCP\IAppConfig;

class CustomSecurityPolicyBuilder {

	public function __construct(
		private IAppConfig $appConfig,
	) {
	}

	public function buildPolicy(): EmptyContentSecurityPolicy {
		$customCSP = $this->getJSONArrayConfigValue();

		$policy = new EmptyContentSecurityPolicy();
		$this->callAllowMethod($policy, $customCSP, 'allowEvalScript');
		$this->callAllowMethod($policy, $customCSP, 'allowInlineStyle');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedScriptDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedStyleDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedFontDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedImageDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedConnectDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedMediaDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedObjectDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedChildSrcDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedFrameDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedFrameAncestorDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedWorkerSrcDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedFormActionDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'reportTo');

		return $policy;
	}

	/**
	 * @param EmptyContentSecurityPolicy $policy
	 * @param array $customCSP
	 * @param string $key
	 * @return void
	 * @psalm-param array<array-key, array<array-key, string>> $customCSP
	 */
	private function callMethodOnDomains(EmptyContentSecurityPolicy $policy, array $customCSP, string $key): void {
		if (isset($customCSP[$key])) {
			$method = $this->convertToMethod($key);
			foreach ($customCSP[$key] as $value) {
				$policy->$method($value);
			}
		}
	}

	private function callAllowMethod(EmptyContentSecurityPolicy $policy, array $customCSP, string $key): void {
		if (isset($customCSP[$key])) {
			$policy->$key($customCSP[$key]);
		}
	}

	private function convertToMethod(string $key): string {
		if ($key === 'reportTo') {
			return 'addReportTo';
		}
		return substr('add' . ucfirst($key), 0, -1);
	}

	/**
	 * @return array
	 * @psalm-return array<array-key,array<array-key, string>>
	 * @throws \JsonException
	 * @throws AppConfigTypeConflictException
	 */
	private function getJSONArrayConfigValue(): array {
		$config = $this->appConfig->getValueString(Application::APP_NAME, 'customCSP', '[]');
		/**
		 * @var array $array
		 * @psalm-var array<array-key,array<array-key, string>> $array
		 */
		$array = json_decode($config, true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
		return $array;
	}
}
