/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
import Vue from 'vue'
import { translate, translatePlural } from '@nextcloud/l10n'
import AdminSettings from './AdminSettings.vue'

Vue.prototype.OC = window.OC
Vue.prototype.OCP = window.OCP
Vue.prototype.t = translate
Vue.prototype.n = translatePlural

export default new Vue({
	el: '#csp-editor-settings',
	render: (h) => h(AdminSettings),
})
