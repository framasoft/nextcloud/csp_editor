<!--
SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>

SPDX-License-Identifier: AGPL-3.0-only
-->

# Changelog

## 1.7.0 - 2025-02-04

### Added

- Support for Nextcloud 31 and 32

### Changed

- Dependencies updates

### Removed

- Support for Nextcloud 28

## 1.6.1 - 2024-10-01

### Added

- Support link to app metadata
- Galician translation

### Changed

- Dependencies updates

## 1.6.0 - 2024-03-13

### Added

- Support for Nextcloud 30
- Translation for Arabic

### Changed

- Upgrade dependencies
- Switch build process from Webpack to Vite

## 1.5.0 - 2024-03-13

### Added

- Support for Nextcloud 29
- Translation for Chinese (Simplified)

## 1.4.0 - 2023-12-19

Node: This is the same release as 1.3.0 with correct compatibility information.

### Added

- Support for Nextcloud 28

### Removed

- Support for Nextcloud 26
- Support for Nextcloud 27

## 1.3.0 - 2023-12-19

### Added

- Support for Nextcloud 28

### Removed

- Support for Nextcloud 26
- Support for Nextcloud 27

## 1.2.2 - 2023-11-07

### Fixed

- Setting not saving because of missing OCP dependency

### Changed

- Upgrade dependencies

## 1.2.1 - 2023-11-03

### Fixed

- Settings not showing because of missing translation dependency

### Changed

- Dependency updates

## 1.2.0 - 2023-10-17

### Added

- Support for Nextcloud 26 and 27
- Support for `child-src` CSP rule

### Changed

- Move all settings to modern Nextcloud Vue components
- Upgrade deps
- Update translations

### Removed

- Support for Nextcloud 24 and 25
- Support for PHP 7.4
- Support for `inline-script` CSP rule (removed by Nextcloud itself)

## 1.1.0 - 2022-08-14

### Added

- Support for Nextcloud 24 and 25.
- Re-enabled checkboxes to allow inline and eval scripts ([since upstream issue is fixed](https://github.com/nextcloud/server/pull/32113))

### Removed

- Support for Nextcloud 22 and 23.
- Support for PHP < 7.4

## 1.0.0 - 2021-12-07

### Added

- Support for Nextcloud 22, 23 and 24.

### Changed

- Disabled allowing inline and eval scripts as it doesn't work because [of a core Nextcloud issue](https://github.com/nextcloud/server/issues/25226).

### Removed

- Support for Nextcloud 19, 20 and 21.


## 0.0.1 - 2020-01-08

Initial release
