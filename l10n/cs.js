OC.L10N.register(
    "csp_editor",
    {
    "CSP Editor" : "CSP Editor",
    "An app to edit Nextcloud's default CSP policy" : "Aplikace pro úpravu výchozí Nextcloud CSP zásady",
    "# Description\n\nThis app allows to edit Nextcloud's default CSP policy." : "# Popis\n\nTato aplikace umožňuje upravovat výchozí zásadu v Nextcloud ohledně CSP.",
    "Allows to edit Nextcloud's default CSP policy." : "Umožňuje upravovat výchozí zásadu v Nextcloud ohledně CSP.",
    "Note: Be extra careful with these settings, and add only what you need below." : "Pozn.: Ohledně těchto nastavení buďte velmi opatrní a níže přidejte pouze to, co potřebujete.",
    "Allow inline script" : "Umožnit inline skript",
    "Allow inline scripts to be executed. Very dangerous. Can be useful for development." : "Umožnit vykonávání inline skriptů. Velmi nebezpečné. Může se hodit pro vývoj.",
    "You need to set \"debug\" => true in your config.php file to allow inline scripts." : "Pokud chcete povolit inline skripty, je třeba v config.php nastavit volbu \"debug\" na hodnotu true.",
    "Allow eval script" : "Umožnit eval skript",
    "Allow scripts to use eval(). Very dangerous. Can be useful for development." : "Umožnit skriptům používat eval(). Velmi nebezpečné. Může se hodit pro vývoj.",
    "You need to set \"debug\" => true in your config.php file to allow to use eval()." : "Pokud chcete povolit používání funkce eval(), je třeba v config.php nastavit volbu \"debug\" na hodnotu true.",
    "Allow inline styles" : "Povolit inline styly",
    "Allow inline styles to be used." : "Povolit použití inline stylů.",
    "Add a domain policy" : "Přidat zásadu ohledně domény",
    "domain.org" : "example.com",
    "Create" : "Vytvořit",
    "Reset all" : "Vrátit vše do výchozího stavu",
    "No custom policy set" : "Nenastavena žádná uživatelsky určená zásada",
    "Pick a policy type and enter a domain" : "Vyberte typ zásady a zadejte doménu",
    "Script" : "Script",
    "Style" : "Styl",
    "Font" : "Písmo",
    "Image" : "Obrázek",
    "Connect" : "Připojit",
    "Media" : "Média",
    "Object" : "Objekt",
    "Frame" : "Rámec",
    "Frame Ancestor" : "Potomek rámce",
    "WorkerSrc" : "WorkerSrc",
    "Form Action" : "Akce formuláře",
    "Report To" : "Nahlásit do",
    "Error while saving custom CSP policy" : "Chyba při ukládání uživatelsky určené CSP zásady"
},
"nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;");
