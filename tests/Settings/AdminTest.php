<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\CSPEditor\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\CSPEditor\AppInfo\Application;
use OCA\CSPEditor\Settings\Admin;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IAppConfig;
use OCP\IConfig;
use PHPUnit\Framework\MockObject\MockObject;

class AdminTest extends TestCase {
	private Admin $admin;
	private IInitialState|MockObject $initialState;
	private IConfig|MockObject $config;
	private IAppConfig|MockObject $appConfig;

	public function setUp(): void {
		parent::setUp();
		$this->config = $this->createMock(IConfig::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->initialState = $this->createMock(IInitialState::class);

		$this->admin = new Admin($this->config, $this->appConfig, $this->initialState);
	}

	public function testGetPriority(): void {
		$this->assertEquals(80, $this->admin->getPriority());
	}

	public function testGetSection(): void {
		$this->assertEquals('additional', $this->admin->getSection());
	}

	/**
	 * @param string $cspString
	 * @param bool $debug
	 * @dataProvider dataForTestGetForm
	 */
	public function testGetForm(string $cspString, bool $debug): void {
		$res = new TemplateResponse(Application::APP_NAME, 'admin');

		$this->appConfig->expects($this->once())->method('getValueString')->with(Application::APP_NAME, 'customCSP', '[]')->willReturn($cspString);
		$this->config->expects($this->once())->method('getSystemValueBool')->with('debug', false)->willReturn($debug);

		$matcher = $this->exactly(2);
		$this->initialState->expects($matcher)->method('provideInitialState')->willReturnCallback(function ($key, $value) use ($matcher, $cspString, $debug): void {
			switch ($matcher->numberOfInvocations()) {
				case 1:
					$this->assertEquals('customCSP', $key);
					$this->assertEquals($cspString, $value);
					break;
				case 2:
					$this->assertEquals('debug', $key);
					$this->assertEquals($debug, $value);
			}
		});

		$this->assertEquals($res, $this->admin->getForm());
	}

	public static function dataForTestGetForm(): array {
		return [
			[
				'[]', false,
				'[]', true
			]
		];
	}
}
