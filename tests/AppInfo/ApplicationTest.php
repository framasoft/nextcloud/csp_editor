<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\CSPEditor\Tests\BackgroundJob;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\CSPEditor\AppInfo\Application;

class ApplicationTest extends TestCase {
	private Application $application;

	public function setUp(): void {
		parent::setUp();
		$this->application = new Application();
	}

	public function testApplicationName(): void {
		$this->assertEquals(Application::APP_NAME, $this->application->getContainer()->getAppName());
	}
}
