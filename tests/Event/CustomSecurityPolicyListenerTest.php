<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\CSPEditor\Tests\Service;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\CSPEditor\Event\CustomSecurityPolicyListener;
use OCA\CSPEditor\Service\CustomSecurityPolicyBuilder;
use OCP\AppFramework\Http\EmptyContentSecurityPolicy;
use OCP\EventDispatcher\Event;
use OCP\Security\CSP\AddContentSecurityPolicyEvent;
use PHPUnit\Framework\MockObject\MockObject;

class CustomSecurityPolicyListenerTest extends TestCase {
	private CustomSecurityPolicyListener $customSecurityPolicyListener;
	private CustomSecurityPolicyBuilder|MockObject $customSecurityPolicyBuilder;

	public function setUp(): void {
		$this->customSecurityPolicyBuilder = $this->createMock(CustomSecurityPolicyBuilder::class);
		$this->customSecurityPolicyListener = new CustomSecurityPolicyListener($this->customSecurityPolicyBuilder);
	}

	public function testWithBadEvent(): void {
		$event = new Event();
		$this->customSecurityPolicyBuilder->expects($this->never())->method('buildPolicy');
		$this->customSecurityPolicyListener->handle($event);
	}

	public function testAddCustomPolicy(): void {
		$policy = new EmptyContentSecurityPolicy();
		$policy->allowEvalScript(true);
		$event = $this->createMock(AddContentSecurityPolicyEvent::class);
		$this->customSecurityPolicyBuilder->expects($this->once())->method('buildPolicy')->willReturn($policy);
		$event->expects($this->once())->method('addPolicy')->with($policy);
		$this->customSecurityPolicyListener->handle($event);
	}
}
