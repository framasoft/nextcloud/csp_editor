<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\CSPEditor\Tests\Service;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\CSPEditor\AppInfo\Application;
use OCA\CSPEditor\Service\CustomSecurityPolicyBuilder;
use OCP\AppFramework\Http\EmptyContentSecurityPolicy;
use OCP\IAppConfig;
use PHPUnit\Framework\MockObject\MockObject;

class CustomSecurityPolicyBuilderTest extends TestCase {

	private IAppConfig|MockObject $appConfig;
	private CustomSecurityPolicyBuilder $customSecurityPolicyBuilder;

	public function setUp(): void {
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->customSecurityPolicyBuilder = new CustomSecurityPolicyBuilder($this->appConfig);
	}

	public function testBuildCustomPolicy(): void {
		$this->appConfig->expects($this->once())->method('getValueString')->with(Application::APP_NAME, 'customCSP', '[]')->willReturn('{"allowInlineStyle":true, "allowedFontDomains": ["something.com", "like.com"], "reportTo": ["someone@somewhere.org"]}');

		$expectedPolicy = new EmptyContentSecurityPolicy();
		$expectedPolicy->allowInlineStyle(true);
		$expectedPolicy->addAllowedFontDomain('something.com');
		$expectedPolicy->addAllowedFontDomain('like.com');
		$expectedPolicy->addReportTo('someone@somewhere.org');

		$this->assertEquals($expectedPolicy, $this->customSecurityPolicyBuilder->buildPolicy());
	}

	public function testBuildCustomPolicyWithEmptyArray(): void {
		$this->appConfig->expects($this->once())->method('getValueString')->with(Application::APP_NAME, 'customCSP', '[]')->willReturn('{}');

		$expectedPolicy = new EmptyContentSecurityPolicy();

		$this->assertEquals($expectedPolicy, $this->customSecurityPolicyBuilder->buildPolicy());
	}
}
